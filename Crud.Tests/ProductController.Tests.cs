﻿using Crud.Controllers;
using Crud.Repositories;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Crud.Tests
{
    [TestFixture]
    public class ProductControllerTests
    {
        [Test]
        public async Task GetProduct_WhenProductExists_ReturnsProduct()
        {
            ProductRepositoryTestMock repoMock = new(new List<Product>
            {
                new Product {Id = 1, Name = "Nesquick", ManufacturerName = "Nestle", AvailableQuantity = 3}
            });

            ProductController controller = new(repoMock, new Mock<ILogger<ProductController>>().Object);

            var product = await controller.GetProduct(1);

            Assert.NotNull(product);
        }
    }
}
