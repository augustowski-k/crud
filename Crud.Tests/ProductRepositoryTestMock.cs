﻿using Crud.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crud.Tests
{
    public class ProductRepositoryTestMock : IProductRepository
    {
        private readonly List<Product> _products;
        public ProductRepositoryTestMock(List<Product> products)
        {
            _products = products;
        }

        public async Task<Product> GetProductAsync(int id)
        {
            return _products.Single(p => p.Id == id);
        }

        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            return _products;
        }

        public async Task<bool> AddProductAsync(Product product)
        {
            if (!_products.Any(p => p.Id == product.Id))
            {
                _products.Add(product);
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> UpdateProductAsync(Product product)
        {
            Product foundProduct = _products.SingleOrDefault(p => p.Id == product.Id);
            if (foundProduct == null)
            {
                return false;
            }

            foundProduct.Name = product.Name;
            foundProduct.ManufacturerName = product.ManufacturerName;
            foundProduct.AvailableQuantity = product.AvailableQuantity;

            return true;
        }
    }
}
