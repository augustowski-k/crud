﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crud.Repositories
{
    public interface IProductRepository
    {
        Task<bool> AddProductAsync(Product product);

        Task<Product> GetProductAsync(int id);

        Task<IEnumerable<Product>> GetProductsAsync();
        Task<bool> UpdateProductAsync(Product product);
    }
}
