﻿using System.ComponentModel.DataAnnotations;

namespace Crud.Repositories
{
    public class Product
    {
        [Range(1, int.MaxValue)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string ManufacturerName { get; set; }

        public int AvailableQuantity { get; set; }
    }
}