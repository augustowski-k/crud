﻿using Crud.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Crud.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        private readonly ILogger _logger;

        public ProductController(IProductRepository productRepository, ILogger<ProductController> logger)
        {
            _productRepository = productRepository;
            _logger = logger;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<Product> GetProduct(int id)
        {
            Product product = await _productRepository.GetProductAsync(id);

            _logger.LogInformation("Returning product with Id: {id} and name: {name}", id, product.Name);

            return product;
        }

        [HttpGet]
        public async Task<IEnumerable<Product>> GetProducts()
        {
            List<Product> products = (await _productRepository.GetProductsAsync()).ToList();

            _logger.LogInformation("Returning {count} products", products.Count);

            return products;
        }

        [HttpPost]
        public async Task<bool> AddProduct(Product product)
        {
            _logger.LogInformation("Adding product: {product}", JsonSerializer.Serialize(product));
            return await _productRepository.AddProductAsync(product);
        }

        [HttpPut]
        public async Task<bool> UpdateProduct(Product product)
        {
            _logger.LogInformation("Updating product: {product}", JsonSerializer.Serialize(product));
            return await _productRepository.UpdateProductAsync(product);
        }
    }
}
